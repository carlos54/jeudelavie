package JDLV;

import java.io.File;
import java.io.FilenameFilter;

/**
 * <b>FiltreFichierLif est la classe permettant de creer un filtre qui n'accepte
 * que les fichier dont l'extension est .LIF ou .lif</b>
 *<p>Elle est utilisee pour lever une erreur dans le cas ou un dossier ne contient aucuns fichier lif</p>
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 * @see  LectureJeu
 */

public class FiltreFichierLif implements FilenameFilter {
	@Override

	public boolean accept(final File dir, final String name) {
		return (name.endsWith(".lif") || name.endsWith(".LIF"));

	}
}
