package JDLV;

/**
 * <b>ModeCirculaire est la classe heritant de la classe Generation et
 * permettant de creer une grille de Jeu de la vie dont les generations
 * suivantes sont calculees avec un comportement circulaire. Ce mode de jeu est
 * borne.</b>
 * 
 *
 * Une Generation ModeCirculaire est une Generation possedant en plus un
 * attribut de type Bornes
 * <ul>
 * <li>Attribut bornes permettant de definir les 4 bornes de la grille</li>
 * 
 * 
 * </ul>
 * 
 * 
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */
public class ModeCirculaire extends Generation {
	/**
	 * Les bornes de la grille
	 */
	private Bornes bornes;

	/**
	 * Construit une Generation ModeCirculaire a partir d'une liste de cellules
	 * vivantes et de bornes
	 * 
	 * @param lc
	 *            La liste qui decrit le Jeu
	 * @param bornes
	 *            les bornes du jeu * @param nbrSurvie Une liste de nombre de
	 *            cellues voisines qu'une cellule vivante doit avoir pour
	 *            survivre le prochain tour
	 * 
	 * 
	 * @param nbrSurvie
	 *            Une liste de nombre de cellues voisines qu'une cellule vivante
	 *            doit avoir pour survivre le prochain tour
	 *
	 * @param nbrNaissance
	 *            Une liste de nombre de cellues voisines qu'une cellule morte
	 *            doit avoir pour naitre le prochain tour
	 */
	public ModeCirculaire(LC<PositionCellule> lc, Bornes bornes, LC<Integer> nbrSurvie, LC<Integer> nbrNaissance) {
		super(lc, nbrSurvie, nbrNaissance);
		this.bornes = bornes;
	}

	/**
	 * Creer et retourne une matrice decale en abscisse et en ordonnee de la
	 * matrice represente par this.listeCellulesVivantes. Afin de respecter la
	 * circularite du jeu les cellules decalees ne se trouvant pas dans le
	 * bornage de la grille sont reintroduites de l'autre cote de la grille du
	 * jeu, d'autant de cases qu'elles en sont sortis.
	 * 
	 * @param x
	 *            premier decalage sur l'axe des abscisses, il peut etre de 1, 0
	 *            ou -1.
	 * @param y
	 *            second decalage sur l'axe des ordonnees, il peut etre de 1,0
	 *            ou -1.
	 * 
	 * @return La liste decale cree
	 */
	@Override
	protected LC<PositionCellule> creerMatriceDecale(int x, int y) {
		LC<PositionCellule> lc = new LC<PositionCellule>();
		Iterator<PositionCellule> it = this.listeCellulesVivantes.iterator();
		while (it.hasNext()) {
			PositionCellule cell = (PositionCellule) it.next();

			int xx = cell.getX() + x;
			int yy = cell.getY() + y;
			// Si une position de cellule depasse les bornes du jeu, on lui
			// donne la position du bord oppose
			if (xx == bornes.getXInf() - 1)
				xx = this.bornes.getXSup();
			if (yy == bornes.getYInf() - 1)
				yy = this.bornes.getYSup();
			if (yy > this.bornes.getYSup())
				yy = this.bornes.getYInf();
			if (xx > this.bornes.getXSup())
				xx = this.bornes.getXInf();
			// on utilise l'ajout trie car meme si la liste this est trie de
			// base, le fait de modifier les positions de cellules d'un bord a
			// l'autre de la grille a pour effet de melanger la liste
			lc.ajoutTrie(new PositionCellule(xx, yy, 1));

		}

		// on ajoute dans lcFinale qui des positionCellules uniques, il faut
		// alors parcourir la liste et supprimer tous les doublons en ajoutant
		// leur valeur dans la positionCellule ajoutee a lcFinale
		LC<PositionCellule> lcFinale = new LC<PositionCellule>();
		Maillon<PositionCellule> maillon = lc.getTete();
		if (maillon != null) {
			lcFinale.ajouterEnTete(maillon.getValeur());
			while (maillon.getSuivant() != null) {
				if (maillon.getValeur().memePosition(maillon.getSuivant().getValeur())) {

					((PositionCellule) lcFinale.getQueue().getValeur())
							.ajouterValeurCellule(maillon.getSuivant().getValeur());
				} else {
					lcFinale.ajouterEnQueue(maillon.getSuivant().getValeur());
				}
				maillon = maillon.getSuivant();
			}
		}
		return lcFinale;
	}

	/**
	 * Retourne une copie du jeu avec de nouvelles adresses. (les nouveaux
	 * elements ne pointent plus vers les elements originaux )
	 * 
	 * @return La copie du jeu.
	 */
	public Generation getCopie() {
		ModeCirculaire jeu = new ModeCirculaire(this.listeCellulesVivantes.getCopie(), this.bornes, this.nbrSurvie,
				this.nbrNaissance);
		return jeu;
	}

	/**
	 * Retourne une representation sous forme de String de la classe
	 * ModeCirculaire. Toutes les cellules se situant a l'interieur du bornage
	 * sont affichees, mortes comme vivantes. Le numero de generation est lui
	 * aussi affiche.
	 * 
	 * @return Une representation sous forme de String d'in objet
	 *         ModeCirculaire.
	 */
	public String toString() {
		String s = "";
		Maillon<PositionCellule> m = this.listeCellulesVivantes.getTete();
		if (m != null)
			// pour toutes les lignes
			for (int y = bornes.getYInf(); y <= bornes.getYSup(); y++) {
				// pour chaque case de chaques lignes
				for (int x = bornes.getXInf(); x <= bornes.getXSup(); x++) {
					// Si une cellule se trouve a la position de la matrice
					// virtuelle qu'on parcourt, alors on affiche une etoile
					// pour representer une cellule vivante
					if (m != null && ((PositionCellule) m.getValeur()).memePosition(new PositionCellule(x, y, 1))) {
						s += ("*");
						m = m.getSuivant();
						// sinon on affiche un point pour representer une
						// cellule morte
					} else
						s += (".");

				} // On saute une ligne a chaque nouvelle ligne de la matrice
					// virtuelle parcourue
				s += ("\n");
			}

		return s += "\nGeneration numero: " + this.getNumeroGeneration() + "\n";

	}
}
