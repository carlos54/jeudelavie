package JDLV;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * <b>La classe LectureJeu est la classe main de ce programme, elle fonctionne
 * selon 6 cas, 5 options rentrees en argument dans un terminale a l'ouverture
 * du dossier, et un cas d'echec, lorsque les parametre rentres sont errones </b>
 *
 * Elle a un seul attribut, l'attribut in de type Scanner qui permet d'effectuer
 * les entrees et sorties pour que le programme communique avec l'utilisateur
 * 
 * 
 * @author Didier Muhire, Charles Sayamath, Thomas Misiek, Anthony Berduck
 */

public class LectureJeu {

	public static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		try {

			switch (args[0]) {
			// cas ou l'utilisateur veut afficher les noms des createurs du
			// programme
			case ("-name"): // on verifie que pour cette option, c'est bien 1
							// paramatre qui a ete rentre
				if (args.length == 1) {
					System.out.println(" Misiek Thomas, Berduck Anthony, Muhire Didier, Sayamath Charles ");
				} else {
					System.out.println("Il ne doit pas y avoir plus de 1 argument pour cette commande");
					System.exit(1);
				}
				break;
			// cas ou l'utilisateur veut connaitre les differentes commandes
			// qu'il peut utiliser
			case ("-h"): // on verifie que pour cette option, c'est bien 1
							// paramatre qui a ete rentre
				if (args.length == 1) {
					System.out.println("-name : Affiche les noms et prenom des auteurs du programme\n");
					System.out.println(
							"-s d fichier.lif : execute une simulation du jeu d�une duree d'affichant les configurations du jeu avec le numero de generation.\n");
					System.out.println(
							"-c max fichier.lif calcule le type d�evolution du jeu avec ses caracteristiques (taille de la queue, periode et deplacement).\n max represente la duree maximale de simulation pour deduire les resultats du calcul. fichierLif represente le chemin d'acces au fichier\n");
					System.out.println(
							"-w max nomDossier calcule le type d�evolution de tous les jeux avec ses caracteristiques (taille de la queue, periode et deplacement).\n max represente la duree maximale de simulation pour deduire les resultats du calcul. nomDossier represente le chemin d'acces au dossier\n");
					System.out.println("");
				} else {
					System.out.println("Il ne doit pas y avoir plus de 1 argument pour cette commande");
					// on renvoie un code d'erreur
					System.exit(1);
				}
				break;
			// cas ou l'utilisateur veut suivre la porgression d'une generation
			// dans le temps
			case ("-s"):
				// on verifie que pour cette option, c'est bien 3 paramatres qui
				// on ete rentres
				if (args.length == 3) {
					// on verifie la validite des arguments rentres en parametre
					// du programme, arret du programme et code d'erreur sinon
					entierValideOuExit(args[1]);
					fichierValideOuExit(args[2]);

					int i = 0;
					{
						String choix = recupererChoixMode();
						// on creer le jeu a partir du fichier lif
						Jeu jeu = creerJeu(choix, new File(args[2]));

						// on affiche une generation de depart, avant de
						// calculer son successeur
						System.out.println(jeu.getGeneration());
						// on boucle tant qu'on a pas atteint la generaiton max
						// demande par l'utilisateur
						while (i < Integer.parseInt(args[1])) {

							// les trois variables de type long qui suivent ont
							// pour but d'afficher a l'utilisateur le temps que
							// prennent l'affichage et le calcul d'une
							// generation
							long startTime = System.currentTimeMillis();

							jeu.getGeneration().successeur();
							long endTime = System.currentTimeMillis();

							long tempsCalcul = (endTime - startTime);
							startTime = System.currentTimeMillis();
							System.out.println(jeu.getGeneration());

							System.out.println(
									"Temps de calcul pour " + jeu.getGeneration().getListeCellulesVivantes().taille()
											+ " cellules: " + (tempsCalcul) + " millisecondes");
							endTime = System.currentTimeMillis();

							System.out.println("Temps d'affichage " + (endTime - startTime) + " millisecondes");
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								System.out.println("Une erreur est survenue, veuillez relancer le programme");
							}

							clearConsole();
							i++;

						}
					}

				} else {
					System.out.println("Cette commande/option doit comporter exactement 3 arguments");
					System.exit(1);
				}
				break;
			// Dans le cas ou l'utilisateur veut afficher le comportement d'un
			// jeu
			case "-c": // on verifie que pour cette option, c'est bien 3
						// paramatres qui on ete rentres
				if (args.length == 3) {
					// on verifie que les parametre rentres par l'utilisateur
					// soient valides
					fichierValideOuExit(args[2]);
					entierValideOuExit(args[1]);

					String choix = recupererChoixMode();
					// on creer un jeu a partir du fichier lif
					Jeu jeu = creerJeu(choix, new File(args[2]));
					// on affiche le toString de l'objet comportement du jeu
					System.out.println(jeu.getComportement(Integer.parseInt(args[1])));

				} else {
					System.out.println("Cette commande/option doit comporter exactement 3 arguments");
					System.exit(1);
				}
				break;
			// dans le cas ou l'utilisateur veut afficher le comportement des
			// fichiers lifs contenus dans un dossier sur une page web
			case "-w":
				// on verifie que pour cette option, c'est bien 3 parametres qui
				// on ete rentres
				if (args.length == 3) {
					// on verifie que les parametre rentres par l'utilisateur
					// soient valides sinon il y a message et code d'erreur et
					// arret du
					// programme
					dossierValideOuExit(args[2]);
					entierValideOuExit(args[1]);
					String dossier = args[2];

					TraitementDossierHtml dossierHtml = new TraitementDossierHtml(dossier);
					// on initialise la liste des jeux a analyse en demandant a
					// l'utilisateur le mode de jeu qu'il desire
					// (circulaire,frontiere, infini)
					dossierHtml.setListeJeux(creerListeJeu(dossierHtml.getListeFichiers(), recupererChoixMode()));
					// on genere le contenu du futur fichier html et on le stock
					// dans la variable html
					String html = dossierHtml.genererHTML(dossier, Integer.parseInt(args[1]));

					try {

						System.out.println(
								"Veuillez selectionner un dossier et donner un nom au fichier que vous voulez creer dans la fenetre qui vient de s'ouvrir");
						// le fichier html est cree
						dossierHtml.enregistrerHtml(html);
						// le fichier html est ouvert
						dossierHtml.ouvrirFenetre();
					} catch (IOException e) {
						System.out.println("Erreur lors de l'ecriture du fichier html");
						System.exit(1);
					}

				} else {
					System.out.println("Cette commande/option doit comporter exactement 3 arguments");
					System.exit(1);
				}

				break;
			default:
				System.out.println(
						"Le premier parametre est errone, pour afficher la liste des options tapez \"java -jar JeuDeLaVie.jar -h\"");
				System.exit(1);
				break;
			}

			System.out.println("Merci d'avoir utilise notre programme a bientot.");
			// on renvoie une valeur de retour pour dire que tout c'est bien
			// passe
			System.exit(0);
		} catch (StackOverflowError e) {
			System.out.println(
					"Le nombre de cellule a traiter est trop grand pour notre programme, il a plante, veuillez generer moins de cellules");
			System.exit(1);
		} catch (Exception e) {
			System.out.println("Le programme a rencontre un probleme inconnu");
			System.exit(-1);
		}

	}

	/**
	 * <p>
	 * Renvoie un code et affiche un message d'erreur si le fichier en parametre
	 * ne corresponds pas a l'extension .lif
	 * </p>
	 * 
	 * @param fichier
	 *            fichier a verifier
	 */
	public static void fichierValideOuExit(String fichier) {
		Path path = Paths.get(fichier);
		if (!((fichier.matches(".*.lif") || fichier.matches(".*.LIF"))
				&& Files.exists(path, LinkOption.NOFOLLOW_LINKS))) {
			System.out.println("Le fichier passe en argument n'existe pas ou n'est pas un fichier lif");
			System.exit(1);
		}

	}

	/**
	 * <p>
	 * Renvoie un code et affiche un message d'erreur si le dossier en parametre
	 * contient pas de fichiers a l'extension .lif ou si le dossier n'existe pas
	 * </p>
	 * 
	 * @param dossier
	 *            le dossier a verifier
	 * 
	 * @see FiltreFichierLif
	 */
	public static void dossierValideOuExit(String dossier) {
		Path path = Paths.get(dossier);
		if (!Files.isDirectory(path)) {
			System.out.println("Le dossier passe en argument n'existe pas");
			System.exit(1);
		} else {
			File dir = new File(dossier);
			if (dir.list(new FiltreFichierLif()).length == 0) {

				System.out.println("Le dossier passe en argument ne contient pas de fichiers lif");
				System.exit(1);
			}
		}
	}

	/**
	 * <p>
	 * Renvoie un code et affiche un message d'erreur si le String en parametre
	 * ne represente pas un entier correct
	 * </p>
	 * 
	 * @param entierPresume
	 *            le String a transformer en entier
	 */
	public static void entierValideOuExit(String entierPresume) {
		try {
			Integer.parseInt(entierPresume);
		} catch (NumberFormatException e) {
			System.out.println("Le second argument devrait etre un entier");
			System.exit(1);
		}
	}

	/**
	 * <p>
	 * Renvoie une liste chaine de jeux d'un mode egal a ceui passe en parametre
	 * correspondants aux jeux decrits par une liste de fichiers lif
	 * </p>
	 * 
	 * @param listeFichier
	 *            Liste de fichiers .lif a partir desquels on va creer la liste
	 *            de jeux
	 * 
	 * @param mode
	 *            Le mode de jeu qu'on va donner a tous les jeux de la liste
	 * 
	 * @return une liste de Jeu
	 * @see LectureJeu#creerJeu(String, File, Bornes)
	 * @see LectureJeu#creerJeu(String, File)
	 */
	public static LC<Jeu> creerListeJeu(LC<File> listeFichier, String mode) {

		LC<Jeu> listeJeu = new LC<Jeu>();
		Iterator<File> it = listeFichier.iterator();

		if (!mode.equals("infini")) {
			if (getInt(
					"Voulez vous donner les bornes pour chaque fichier lif (Tapez 1) ou pour tous les fichier d'un coup (Tapez 2)",
					1, 2) == 1) {
				// pour tous les jeux
				while (it.hasNext()) {
					File f = (File) it.next();
					// on demande les bornes a chaque fois dans la methode
					// creerJeu
					System.out.println("Choisissez les bornes du fichier " + f.getName());
					listeJeu.ajouterEnQueue(creerJeu(mode, (File) it.next()));
				}

			} else {
				System.out.println("Veuillez choisir les bornes qui s'appliqueront a tous les fichiers d'un coup");
				Bornes bornes = choixBorne();
				while (it.hasNext()) {
					listeJeu.ajouterEnQueue(creerJeu(mode, (File) it.next(), bornes));
				}
			}
		}

		else {
			// creation d'une liste de jeux infinis, pas besoin de demander les
			// bornes
			while (it.hasNext()) {

				listeJeu.ajouterEnQueue(creerJeu(mode, (File) it.next()));
			}
		}
		return listeJeu;
	}

	/**
	 * <p>
	 * Rafraichit la console, et permet donc un affichage des generations
	 * successives plus propre et sans decalage
	 * </p>
	 */
	public static void clearConsole() {

		System.out.println("\033[H\033[2J");
		System.out.flush();

	}

	/**
	 * <p>
	 * Recupere le choix du mode de jeu de l'utilisateur, repose la question
	 * tant qu'un des 3 modes de jeu n'est pas choisi, que la reponse de
	 * l'tuilisateur ne satisfait pas le format de reponse accepte, es robuste,
	 * ne provoque pas d'erreur quoi que l'utilisateur rentre dans la console
	 * </p>
	 * 
	 * @return Le choix de l'utilisateur concernant le mode de jeu
	 */

	public static String recupererChoixMode() {
		System.out.println("");
		System.out.println("Voulez vous un univers avec frontieres (1) infini (2) ou circulaire (3) ?");

		int choix = 0;
		String mode = "";
		do {
			try {
				choix = in.nextInt();
			} catch (Exception E) {
				in.nextLine();
			}

			switch (choix) {
			case 1:
				mode = "frontiere";
				break;
			case 2:
				mode = "infini";
				break;
			case 3:
				mode = "circulaire";
				break;
			default:
				System.out.println("Veuillez entrer un nombre entre 1 et 3");
			}
		} while (mode == "");

		return mode;
	}

	/**
	 * Demande a l'utilisateur de creer 4 bornes avec lesquels on instancie et
	 * retourne un objet Bornes
	 * 
	 * @return La bornes instanciee
	 * @see LectureJeu#getInt(String)
	 */
	public static Bornes choixBorne() {

		int x1 = getInt("Veuillez choisir la premiere borne des abscisses");

		int x2 = getInt("Veuillez choisir la seconde borne des abscisses");

		int y1 = getInt("Veuillez choisir la premiere borne des ordonnees");

		int y2 = getInt("Veuillez choisir la seconde borne des ordonnees");
		return new Bornes(x1, x2, y1, y2);
	}

	/**
	 * renvoie la reponse comprise entre deux entiers sous forme d'entier d'un
	 * utilisateur a une question donnee
	 * 
	 * @param question
	 *            question donnee
	 * @param min
	 *            enter minimum
	 * @param max
	 *            entier maximum
	 * @return reponse de l'utilisateur
	 */
	public static int getInt(String question, int min, int max) {
		int a = 0;
		boolean b = false;
		// la question est reposee tant que la valeur donnee par l'utilisateur
		// n'est pas conforme a ce qui est attendu
		while (a < min || a > max || !b) {
			b = true;
			System.out.println(question);
			try {
				a = in.nextInt();
				in.nextLine();
			} catch (Exception e) {
				System.out.println("Veuillez saisir un entier compris entre " + min + " et " + max);
				in.nextLine();
				b = false;
			}

		}
		return a;
	}

	/**
	 * renvoie la reponse sous forme d'entier d'un utilisateur a une question
	 * donnee
	 * 
	 * @param question
	 *            question donnee
	 * 
	 * @return reponse de l'utilisateur
	 */
	public static int getInt(String question) {
		int a = 0;
		boolean b = false;
		while (!b) {
			System.out.println(question);
			b = true;

			try {
				a = in.nextInt();
				in.nextLine();
			} catch (Exception e) {
				System.out.println("Veuillez saisir un entier");
				b = false;
				in.nextLine();
			}
		}
		return a;
	}

	/**
	 * Instancie et retourne un jeu
	 * <p>
	 * Les bornes sont demandees au cours de l'execution de la methode
	 * contrairement a la methode creerJeu(String,File,Bornes)
	 * </p>
	 * 
	 * @param mode
	 *            mode de jeu
	 * @param fichier
	 *            Fichier a partir duquel on creer le jeu
	 * @return Le jeu cree
	 * @see LectureJeu#creerListeJeu(LC, String)
	 * @see LectureJeu#creerJeu(String, File, Bornes)
	 */
	public static Jeu creerJeu(String mode, File fichier) {

		if (mode.equals("infini"))

			return new Jeu(fichier, mode, null);

		else {

			return new Jeu(fichier, mode, choixBorne());

		}
	}

	/**
	 * Instancie et retourne un jeu
	 * <p>
	 * Les bornes sont deja en parametre contrairement a la methode
	 * creerJeu(String,File)
	 * </p>
	 * 
	 * @param mode
	 *            mode de jeu
	 * @param fichier
	 *            Fichier a partir duquel on creer le jeu
	 * 
	 * @param bornes
	 *            Les bornes du jeu
	 * @return Le jeu cree
	 * @see LectureJeu#creerListeJeu(LC, String)
	 * @see LectureJeu#creerJeu(String, File)
	 */
	public static Jeu creerJeu(String mode, File fichier, Bornes bornes) {

		return new Jeu(fichier, mode, bornes);

	}

}