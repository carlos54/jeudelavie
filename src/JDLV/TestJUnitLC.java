package JDLV;
import java.lang.reflect.Field;
import org.junit.*;

/**
 * <b>TestJUnitLC est la classe permettant de tester la validit� des m�thodes
 * de la classe LC</b>
 * 
 * 
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */

public class TestJUnitLC {
		
	
	/**
	 * Verifie si la methode getTete renvoie bien la tete de la liste
	 * @throws NoSuchFieldException pour l'objet Field
	 * @throws IllegalAccessException pour l'objet Field
	 */
	@Test
	public void testGetTete()  throws NoSuchFieldException, IllegalAccessException {
		LC<Integer> liste = new LC<Integer>();
		Field field=liste.getClass().getDeclaredField("tete");
		field.setAccessible(true);
		Maillon<Integer> m=new Maillon<Integer>(2,null);
		field.set(liste,m);
		Maillon<Integer> result=liste.getTete();
		Assert.assertEquals(m,result);
	}
	
	/**
	 * Verifie si la methode getQueue renvoie bien la queue de la liste
	 * @throws NoSuchFieldException pour l'objet Field
	 * @throws IllegalAccessException pour l'objet Field
	 */
	
	@Test
	public void testGetQueue() throws NoSuchFieldException, IllegalAccessException {
		LC<Integer> liste = new LC<Integer>(1);
		Field field=liste.getClass().getDeclaredField("queue");
		field.setAccessible(true);
		Maillon<Integer> m=new Maillon<Integer>(2,null);
		field.set(liste,m);
		Maillon<Integer> result=liste.getQueue();
		Assert.assertEquals(m,result);
	}
	
	
	/**
	 * Verifie si la methode ajouterEnTete ajoute bien un maillon en tete 
	 * 
	 */
	
	@Test
	public void testAjouterEnTete() {
		LC<Integer> liste = new LC<Integer>(2);
		Maillon<Integer> m=liste.getTete();
		liste.ajouterEnTete(3);
		Assert.assertNotEquals(m,liste.getTete());
		Assert.assertTrue(3 == liste.get(0));
	}
	
	
	/**
	 * Verifie si la methode ajouterEnQueue ajoute bien un maillon en queue
	 * 
	 */
	
	@Test
	public void testAjouterEnQueue() {
		LC<Integer> liste = new LC<Integer>(2);
		Maillon<Integer> m=liste.getQueue();
		liste.ajouterEnQueue(3);
		Assert.assertNotEquals(m,liste.getQueue());
		Assert.assertTrue(3 == liste.get(1));
	}
	
	/**
	 * Verifie si la methode equals renvoie bien l'�galit� de deux listes
	 */
	
	@Test
	public void testEquals() {
		LC<Integer> l1 =new LC<Integer>();
		LC<Integer> l2 =new LC<Integer>();
		Assert.assertTrue(l1.equals(l2));
		l1.ajouterEnTete(2);
		Assert.assertFalse(l1.equals(l2));
	}
	
	/**
	 * Verifie si la methode getCopie renvoie bien une copie d'une liste quelconque
	 */
	
	@Test
	public void testGetCopie() {
		LC<Integer> l =new LC<Integer>();
		Assert.assertTrue(l.equals(l.getCopie()));
	}
	
	/**
	 * V�rifie si la methode appartientListe renvoie le bon booleen en fonction de la situation
	 */
	
	@Test
	public void testAppartientListe() {
		LC<Integer> l1 =new LC<Integer>();
		l1.ajouterEnTete(3);
		Assert.assertEquals(false,l1.appartientListe(2));
		Assert.assertEquals(true,l1.appartientListe(3));
	}
	
	/**
	 * Verifie si la methode taille renvoie bien la bonne taille de la liste en fonction du nombre de maillon
	 * 
	 */
	
	@Test
	public void testTaille() {
		LC<Integer> l1 =new LC<Integer>();
		Assert.assertEquals(0, l1.taille());
		l1.ajouterEnTete(3);
		Assert.assertEquals(1,l1.taille());
	}
	
	/**
	 * Verifie si la methode ajoute et trie les maillons dans l'ordre que l'on souhaite
	 */
	
	@Test
	public void testAjoutTrie(){
		LC<Integer> l1 =new LC<Integer>(3);
		l1.ajoutTrie(5);
		l1.ajoutTrie(10);
		l1.ajoutTrie(1);
		LC<Integer>expectedList=new LC <Integer>(5);
		expectedList.ajouterEnTete(3);
		expectedList.ajouterEnTete(1);
		expectedList.ajouterEnQueue(10);
		for(int i = 0 ; i<4; i++) {
			Assert.assertEquals(l1.get(i),expectedList.get(i));
		}
	}
	
	/**
	 * Verifie si la methode ajoutTrieSansDoublon ajoute et trie les maillons dans l'ordre que l'on souhaite
	 * @throws Exception pour ajoutTrieSansDoublon
	 */
	
	
	@Test
	public void testAjoutTrieSansDoublon() throws Exception  {
		LC<Integer> l1 =new LC<Integer>(3);
		l1.ajoutTrieSansDoublon(5);
		l1.ajoutTrieSansDoublon(10);
		l1.ajoutTrieSansDoublon(1);
		LC<Integer>expectedList=new LC <Integer>(5);
		expectedList.ajouterEnTete(3);
		expectedList.ajouterEnTete(1);
		expectedList.ajouterEnQueue(10);
		for(int i = 0 ; i<4; i++) {
			Assert.assertEquals(l1.get(i),expectedList.get(i));
		}
	}
	
	/**
	 * V�rifie si la m�thode renvoie une exception si l'on essaye d'ajouter un maillon d'une valeur existant d�j� dans la liste
	 */
	
	@Test
	public void testExceptionAjoutTrieSansDoublon() {
		try {
			LC<Integer> l1 =new LC<Integer>(2);
			l1.ajoutTrieSansDoublon(2);
			Assert.fail("Exception attendue en ajoutant un maillon ayant la m�me valeur qu'un existant d�j�");
		}
		catch(Exception e){
			//on attend rien car c'est le comportement attendu
		}
	}
	
	/**
	 * Verifie si la liste ne contient aucun element
	 */
	
	@Test
	public void testListeVide() {
		LC<Integer> liste = new LC<Integer>();
		Assert.assertEquals(true, liste.estListeVide());
		liste.ajouterEnTete(1);
		Assert.assertEquals(false, liste.estListeVide());
	}
	
	
	/**
	 * Verifie si la methode Iterator renvoie bien un iterateur de la liste
	 */
	
	@Test
	public void testIterator() {
		LC<Integer> liste = new LC<Integer>();
		Assert.assertNotNull(liste.iterator());
	}
	
	/**
	 * Verifie si la methode get renvoie bien la valeur du ieme maillon que l'on souhaite
	 */
	
	@Test
	public void testGet(){
		LC<Integer> liste = new LC<Integer>(3);
		Assert.assertTrue(liste.get(0)==3);	
		liste.ajouterEnTete(1);
		Assert.assertTrue(liste.get(0)==1);	
	}
	
	/**
	 * Verifie si la methode toString renvoie bien la representation de la liste que l'on souhaite
	 */
	
	@Test
	public void testToString() {
		LC<Integer> liste = new LC<Integer>();
		Assert.assertEquals(liste.toString(),"");
		liste.ajouterEnTete(1);
		Assert.assertEquals("1\n",liste.toString());
	}
	
}
