package JDLV;

/**
 * <b>ModeInfini est la classe heritant de la classe Generation et permettant de
 * creer une grille de Jeu de la vie avec un comportement infini. Ce mode de jeu
 * n'est pas borne.</b>
 * 
 * <p>
 * Une Generation ModeInfini ne possede pas d'attribut en plus que la classe
 * Generation dont il herite
 * 
 * </p>
 * 
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */
public class ModeInfini extends Generation {

	/**
	 * Constructeur de la generation ModeInfini.
	 * 
	 * <p>
	 * Fait appel au constructeur de la classe Generation. Creer une generation
	 * dont les generations suivantes sont calculee sur une matrice infini a
	 * partir d'une liste de PositionsCellules.
	 * </p>
	 * 
	 * @param lc
	 *            La liste qui decrit le Jeu
	 * 
	 * @param nbrSurvie
	 *            Une liste de nombre de cellues voisines qu'une cellule vivante
	 *            doit avoir pour survivre le prochain tour
	 * @param nbrNaissance
	 *            Une liste de nombre de cellues voisines qu'une cellule morte
	 *            doit avoir pour naitre le prochain tour
	 */
	public ModeInfini(LC<PositionCellule> lc, LC<Integer> nbrSurvie, LC<Integer> nbrNaissance) {

		super(lc, nbrSurvie, nbrNaissance);
	}

	/**
	 * <p>Creer et retourne une matrice decalee en abscisse et en ordonne de la
	 * matrice represente par this.listeCellulesVivantes.</p>
	 * 
	 * @param x
	 *            premier decalage sur l'axe des abscisses, il peut etre de 1, 0
	 *            ou -1.
	 * @param y
	 *            second decalage sur l'axe des ordonnees, il peut etre de 1,0
	 *            ou -1.
	 * 
	 * @return La liste decale cree
	 */
	@Override
	protected LC<PositionCellule> creerMatriceDecale(int x, int y) {
		LC<PositionCellule> lc = new LC<PositionCellule>();
		Iterator<PositionCellule> it = this.listeCellulesVivantes.iterator();
		while (it.hasNext()) {
			PositionCellule cell = (PositionCellule) it.next();

			lc.ajouterEnQueue(new PositionCellule(cell.getX() + x, cell.getY() + y, 1));

		}
		return lc;
	}

	/**
	 * <p>Retourne une copie du jeu avec de nouvelles adresses. (les nouveaux
	 * elements ne pointent plus vers les elements originaux )</p>
	 * 
	 * @return La copie du jeu.
	 */
	public Generation getCopie() {
		ModeInfini jeu = new ModeInfini(this.listeCellulesVivantes.getCopie(), nbrSurvie, nbrNaissance);
		return jeu;
	}
}
