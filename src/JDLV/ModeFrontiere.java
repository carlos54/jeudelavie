package JDLV;

/**
 * <b>ModeFrontiere est la classe heritant de la classe Generation et permettant
 * de creer une grille de Jeu de la vie dont les cotes sont bornes.</b>
 * 
 * 
 * Une Generation ModeFrontiere est une Generation possedant en plus un attribut
 * de type Bornes
 * <ul>
 * <li>Attribut bornes permettant de definir les 4 bornes de la grille</li>

 * 
 * </ul>
 * 
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */
public class ModeFrontiere extends Generation {
	/**
	 * Les bornes de la grille
	 */
	private Bornes bornes;

	/**
	 * Constructeur de la Generation ModeFrontiere. Creer une Generation
	 * ModeFrontiere a partir d'une liste de cellules vivantes et de bornes
	 * 
	 * @param lc
	 *            La liste qui decrit le Jeu
	 * @param bornes
	 *            les bornes du jeu
	 * @param nbrSurvie
	 *            Une liste de nombre de cellues voisines qu'une cellule vivante
	 *            doit avoir pour survivre le prochain tour
	 * @param nbrNaissance
	 *            Une liste de nombre de cellues voisines qu'une cellule morte
	 *            doit avoir pour naitre le prochain tour
	 */
	public ModeFrontiere(LC<PositionCellule> lc, Bornes bornes, LC<Integer> nbrSurvie, LC<Integer> nbrNaissance) {

		super(lc, nbrSurvie, nbrNaissance);
		this.bornes = bornes;
	}

	/**
	 * Creer et retourne une matrice decale en abscisse et en ordonnes de la
	 * matrice representee par this.listeCellulesVivantes. Les cellules decalees
	 * se trouvant a l'exterieur du bornage sont ne sont pas ajoutees a la
	 * matrice decale renvoyee sous forme de liste.
	 * 
	 * 
	 * @param x
	 *            premier decalage sur l'axe des abscisses, il peut etre de 1, 0
	 *            ou -1.
	 * @param y
	 *            second decalage sur l'axe des ordonnees, il peut etre de 1,0
	 *            ou -1.
	 * 
	 * @return La liste decale cree
	 * 
	 * @see ModeFrontiere#successeur()
	 * @see Jeu#listeVoisinsVivants()
	 */
	@Override
	protected LC<PositionCellule> creerMatriceDecale(int x, int y) {
		LC<PositionCellule> lc = new LC<PositionCellule>();
		Iterator<PositionCellule> it = this.listeCellulesVivantes.iterator();
		while (it.hasNext()) {
			PositionCellule cell = (PositionCellule) it.next();
			// on creer une nouvelle cellule decalee par rapport a la cellule
			// itere de la liste
			PositionCellule cellule = new PositionCellule(cell.getX() + x, cell.getY() + y, 1);
			if (this.bornes.contient(cellule))
				lc.ajouterEnQueue(cellule);

		}
		return lc;
	}

	/**
	 * Retourne une copie du jeu avec de nouvelles adresses. (les nouveaux
	 * elements ne pointent plus vers les elements originaux )
	 * 
	 * @return La copie du jeu.
	 */
	public Generation getCopie() {
		ModeFrontiere jeu = new ModeFrontiere(this.listeCellulesVivantes.getCopie(), this.bornes, nbrSurvie,
				nbrNaissance);
		return jeu;
	}

	/**
	 * Retourne une representation sous forme de String de la classe
	 * ModeFrontiere. Toutes les cellules se situant a l'interieur du bornage
	 * sont affichees, mortes comme vivantes. Le numero de generation est lui
	 * aussi affiche
	 * 
	 * @return Une representation sous forme de String d"un objet ModeFrontiere.
	 */
	public String toString() {
		String s = "";
		Maillon<PositionCellule> m = this.listeCellulesVivantes.getTete();
		if (m != null)
			// pour toutes les lignes
			for (int y = bornes.getYInf(); y <= bornes.getYSup(); y++) {
				// pour chaque case de chaques lignes
				for (int x = bornes.getXInf(); x <= bornes.getXSup(); x++) {
					// Si une cellule se trouve a la position de la matrice
					// virtuelle qu'on parcourt, alors on affiche une etoile
					// pour representer une cellule vivante
					if (m != null && ((PositionCellule) m.getValeur()).memePosition(new PositionCellule(x, y, 1))) {
						s += ("*");
						m = m.getSuivant();
						// sinon on affiche un point pour representer une
						// cellule morte
					} else
						s += (".");

				} // On saute une ligne a chaque nouvelle ligne de la matrice
					// virtuelle parcourue
				s += ("\n");
			}

		return s += "\nGeneration numero: " + this.getNumeroGeneration() + "\n";

	}

}
