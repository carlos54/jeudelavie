package JDLV;
/**
 * <b> TraitementDossierHtml est la classe permet de gerer la creation du fichier html.</b>
 * <p>
 * TraitementDossierHtml est caracterise par 4 attributs:
 * <ul>
 * <li>La liste des fichiers contenus dans le dossier </li>
 * <li>La liste de jeux creer a partir du dossier </li>
 * <li>Le chemin d'acces au dossier </li>
 * <li>Le fichier dans lequel on enregistre la liste des comportement en .html</li>
 * </ul>
 * </p>
 * @authors Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

public class TraitementDossierHtml {
	/**
	 * La liste de fichiers lif contenus dans le dossier
	 */
	private LC<File> listeFichiers;
	/**
	 * La liste de jeux crees a partir du fichier lif du dossier
	 */
	private LC<Jeu> listeJeux;
	/**
	 *
	 * Le fichier dans lequel on enregistre la liste des comportement en .html
	 * des fichiers du dossier a analyser
	 */

	private File fichierEnregistrement;

	/** Le chemin du dossier sous forme de String */
	private String dirPath;

	/**
	 * Construit un objet TraitementDossierHtml
	 * 
	 * @param dossier
	 *            chemin du dossier sous forme de String
	 */

	public TraitementDossierHtml(String dossier) {
		this.listeFichiers = chargerDossier(dossier);
		this.dirPath = dossier;
	}

	/**
	 * Mutateur de ListeJeux
	 * 
	 * @param listeJeux
	 *            la nouvelle liste de jeux
	 * @throws Exception
	 *             Si la liste de jeux est vide
	 */

	public void setListeJeux(LC<Jeu> listeJeux) throws Exception {
		if (listeJeux.estListeVide())
			throw new Exception("La liste de jeux est vide");
		else
			this.listeJeux = listeJeux;
	}

	/**
	 * Accesseur de listeFichiers
	 * 
	 * @return La liste des fichiers
	 */
	public LC<File> getListeFichiers() {
		return listeFichiers;
	}

	/**
	 * Accesseur de listeJeux
	 * 
	 * @return La liste des jeux
	 */
	public LC<Jeu> getListeJeux() {
		return listeJeux;
	}

	/**
	 * Accesseur de dirPath
	 * 
	 * @return Le chemin du dossier
	 */
	public String getDirPath() {
		return dirPath;
	}

	/**
	 * <p>
	 * Ouvre une fenetre web avec le resultat du calcul des comportements de
	 * tous les fichiers lif d'un dossier
	 * </p>
	 * 
	 * @throws IOException
	 *             Si le fichier que l'on veut ouvrir n'est pas valide
	 */
	public void ouvrirFenetre() throws IOException {
		Desktop desktop = Desktop.getDesktop();

		desktop.open(this.fichierEnregistrement);

	}

	/**
	 * Charge le dossier contenant les fichiers lif
	 * 
	 * @param dossier
	 *            le chemin du dossier
	 * @return Tous les fichiers lifs du dossier
	 */
	private static LC<File> chargerDossier(String dossier) {

		File mondossier = new File(dossier);
		File[] mesFichiers = mondossier.listFiles();
		LC<File> fichierLif = new LC<File>();
		for (int i = 0; i < mesFichiers.length; i++) {
			if ((Pattern.compile("[a-zA-Z0-9].LIF$").matcher(mesFichiers[i].toString()).find())
					|| Pattern.compile("[a-zA-Z0-9].lif$").matcher(mesFichiers[i].toString()).find()) {
				fichierLif.ajouterEnQueue(new File(mesFichiers[i].getPath()));
			}
		}

		return fichierLif;
	}

	/**
	 * Creer tout le contenu du fichier html puis l'enregistre grace a la classe
	 * enregistrerHTML()
	 * 
	 * @param dirPath
	 *            chemin du dossier
	 * @param duree
	 *            nombre de generation maximale a effectuer
	 * @return le contenu d'un fichier html qui contient le comportement de tous
	 *         les jeux
	 * @see TraitementDossierHtml#creerLigneTableau(Jeu, int)
	 */
	public String genererHTML(String dirPath, int duree) {

		String style = "\t\t<style type=\"text/css\">\n" + "\t\t\thtml{ font-family:\"Segoe UI\",Arial}\n"
				+ "\t\t\ttable {border-collapse: collapse;}\n" + "\t\t\ttd {height: 30px;vertical-align: bottom;}\n"
				+ "\t\t\tth, td {width:150px;vertical-align:middle; text-align: left; border-bottom: 1px solid #ddd;}\n"
				+ "\t\t\ttr:nth-child(2n) {background-color: #f2f2f2;}\n"
				+ "\t\t\th1{font-weight:normal;font-size: 1.2em;margin-bottom:50px;}\n" + "\t\t</style>\n\n";

		String html = "<!DOCTYPE html>\n\n" + "<html>\n\n" + "\t<head>\n\n" + style + "\t</head>\n\n" + "\t<body>\n\n";

		String titre = "\t\t<h1>Evolution des fichiers .lif du dossier " + dirPath
				+ " en un temps maximal de recherche du pattern de " + duree + " iterations </h1>\n\n";
		String title = "\t\t<title> Jeu de la vie</title>\n\n";

		String table = "\t\t<table>\n" + "\t\t\t<tr>\n" + "\t\t\t\t<th>Nom du fichier</th>\n"
				+ "\t\t\t\t<th>Type</th>\n" + "\t\t\t\t<th>Taille de la queue</th>\n" + "\t\t\t\t<th>Periode</th>\n"
				+ "\t\t\t\t<th>Deplacement sur l'axe des abscisses</th>\n"
				+ "\t\t\t\t<th>Deplacement sur l'axe des ordonnees</th>\n" + "\t\t\t</tr>\n\n";

		String tableau = "";
		Iterator<Jeu> it = this.listeJeux.iterator();
		while (it.hasNext()) {
			Jeu monJeu = (Jeu) it.next();
			tableau += creerLigneTableau(monJeu, duree);
		}
		html += titre + title + table + tableau + "\t\t</table>\n\n\t</body>\n\n</html>";
		return html;

	}

	/**
	 * Cree une nouvelle ligne de tableau avec les informations correspondants
	 * au fichier lif en fonction du nombre de generation maximale a effectuer
	 * (duree)
	 * 
	 * @param jeu
	 *            Le jeu pour lequel on va recuperer le type de comportement, la
	 *            periode...
	 * @param duree
	 *            int nombre de generation maximale a effectuer pour calculer le
	 *            comportement
	 * @return Une ligne de tableau comportant des informations sur le
	 *         comportement du jeu
	 * @see TraitementDossierHtml#genererHTML(String, int)
	 */
	private String creerLigneTableau(Jeu jeu, int duree) {

		Comportement pattern = jeu.getComportement(duree);
		if (!pattern.getType().equals("Inconnu"))
			return "\t\t\t<tr>\n\t\t\t\t<td>" + jeu.getFileName() + "</td>\n\t\t\t\t<td>" + pattern.getType()
					+ "</td>\n\t\t\t\t<td>" + pattern.getQueue() + "</td>\n\t\t\t\t<td>" + pattern.getPeriode()
					+ "</td>\n\t\t\t\t<td>" + pattern.getDeplacementX() + "</td>\n\t\t\t\t<td>"
					+ pattern.getDeplacementY() + "</td>\n\t\t\t\t</tr>\n\n";
		else
			return "\t\t\t<tr>\n\t\t\t\t<td>" + jeu.getFileName() + "</td>\n\t\t\t\t<td>Inconnu</td>"
					+ "\n\t\t\t\t<td>Aucune</td>" + "\n\t\t\t\t<td>Aucune</td>" + "\n\t\t\t\t<td>Aucun</td>"
					+ "\n\t\t\t\t<td>Aucun</td>" + "\n\t\t\t</tr>\n\n";

	}

	/**
	 * Enregistre les informations sur le comportement de tous les jeu du
	 * dossier dans un fichier html que l'utilisateur choisis
	 * 
	 * @param contenu
	 *            Le code html qu'on veut placer dans un fichier
	 * 
	 * 
	 * @throws IOException
	 *             Si on ne peut pas ouvrir le fichier selectionner par
	 *             l'utilisateur
	 * @see FolderFilter
	 */

	public void enregistrerHtml(String contenu) throws IOException {
		// attention JFileChooser ne permet pas de s'afficher devant les autres
		// fenetres, l'experience utilisateur peut donc etre perturbee

		// JFileCHooser va demander a l'utilisateur de choisir un dossier ou
		// enregistrer son fichier html et il va lui demander de donner un nom
		// au fichier il ne rese plus qu'a mettre l'extension html

		JFileChooser f = new JFileChooser();
		f.setDialogTitle("Enregistrer sous");
		f.setFileSelectionMode(JFileChooser.FILES_ONLY);
		f.setFileFilter(new FolderFilter());
		f.showSaveDialog(null);

		String fichierEnregistrement = f.getSelectedFile().toString() + ".html";

		BufferedWriter bw = new BufferedWriter(new FileWriter(fichierEnregistrement));
		this.fichierEnregistrement = new File(fichierEnregistrement);
		PrintWriter ecrire = new PrintWriter(bw);
		ecrire.write(contenu);
		ecrire.close();

	}
}
