package JDLV;

/**
 * <b>LC est la classe permettant de creer un une liste chainee generique a
 * partir d'un ou de plusieurs maillons.</b>
 * 
 * 
 * Une liste chainee est caracterise par les attributs suivants
 * <ul>
 * <li>Un attribut tete permmettant d'acceder a la liste par son debut</li>
 * <li>Un attribut fin permettant d'acceder a la liste par sa fin</li>
 * </ul>
 * 
 * 
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */

public class LC<T> {

	/** La tete de la liste */
	private Maillon<T> tete;
	/** La fin de la liste */
	private Maillon<T> queue;

	/** Constructeur par defaut de LC */
	public LC() {
		this.tete = null;
		this.queue = null;
	}

	/**
	 * Constructeur de LC qui fixe la valeur du premier et dernier maillon de la
	 * liste.
	 * 
	 * @param m
	 *            La valeur du premier maillon.
	 */
	public LC(T m) {
		Maillon<T> maillon = new Maillon<T>(m, null);
		this.tete = maillon;
		this.queue = maillon;
	}

	/**
	 * Retourne le maillon de tete.
	 * 
	 * @return Le maillon de tete.
	 */
	public Maillon<T> getTete() {
		return this.tete;
	}

	/**
	 * Ajoute un element au debut de la liste.
	 * 
	 * @param v
	 *            Element qui va etre ajoute en tete de liste.
	 */
	public void ajouterEnTete(T v) {
		if (this.estListeVide()) {
			this.tete = new Maillon<T>(v, null);
			this.queue = this.tete;
		} else
			this.tete = new Maillon<T>(v, this.tete);
	}

	/**
	 * Retourne le maillon de queue de liste.
	 * 
	 * @return Le maillon de queue de liste.
	 */
	public Maillon<T> getQueue() {
		return this.queue;
	}

	/**
	 * Ajoute un element a la fin de la liste.
	 * 
	 * @param v
	 *            Element qui va etre ajoute en fin de liste.
	 */
	public void ajouterEnQueue(T v) {
		if (this.estListeVide())
			ajouterEnTete(v);
		else {
			Maillon<T> m = new Maillon<T>(v, null);
			this.queue.setSuivant(m);
			this.queue = m;
		}
	}

	/**
	 * Verifie si l'objet en parametre et l'objet this sont egaux.
	 * 
	 * @param obj
	 *            L'objet dont il faut verifie l'egalite avec this.
	 * 
	 * @return False si les deux objets ne sont pas egaux, True si ils le sont.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LC<T> other = (LC) obj;
		if (queue == null) {
			if (other.queue != null)
				return false;
		} else {
			if (!queue.equals(other.queue)) {

				return false;
			}
		}

		if (tete == null) {
			if (other.tete != null)
				return false;
		} else if (!tete.equals(other.tete))
			return false;
		return true;
	}

	/**
	 * Retourne une copie de l'objet this, dont tous les elements constituants
	 * sont des attributs primaires et non des pointeurs.
	 * 
	 * @return La liste copie.
	 * 
	 */
	public LC<PositionCellule> getCopie() {
		LC<PositionCellule> copieLC = new LC<>();

		Iterator<T> it = this.iterator();
		while (it.hasNext()) {
			PositionCellule pc = (PositionCellule) it.next();
			copieLC.ajouterEnQueue(new PositionCellule(pc.getX(), pc.getY(), pc.getVal()));

		}
		return copieLC;

	}

	/**
	 * Retourne si une liste est vide ou non.
	 *
	 * @return True si la liste et vide, False sinon.
	 */
	public boolean estListeVide() {

		return this.tete == null;

	}

	/**
	 * Retourne si une valeur appartient a une liste chainee.
	 *
	 * @param x
	 *            La valeur a chercher.
	 * @return True si la valeur est dans la liste, False sinon.
	 */

	public boolean appartientListe(T x) {
		Maillon<T> m;
		m = this.tete;
		while (m != null && !m.getValeur().equals(x))
			m = m.getSuivant();
		return m != null;
	}

	/**
	 * Donne le nombre de maillon de la chaine.
	 *
	 * @return entier correspondant au nombre de maillons de la chaine.
	 * 
	 *
	 */
	public int taille() {
		if (!this.estListeVide()) {

			Iterator<T> it = this.iterator();
			int n = 0;
			while (it.hasNext()) {
				it.next();
				n++;

			}
			return n;
		} else
			return 0;

	}

	/**
	 * Ajoute un maillon a partir de sa valeur a la liste chainee de maniere
	 * triee par rapport a un ordre defini dans la classe T. Elle fait appel a
	 * une methode auxillaire.
	 *
	 * @param x
	 *            valeur du maillon a ajouter
	 * @see LC#ajoutTrie(Maillon, Maillon)
	 */
	public <T extends Comparable<? super T>> void ajoutTrie(T x) {

		Maillon<T> m = new Maillon<T>(x, null);

		if (!this.estListeVide()) {

			if (((Comparable<? super T>) this.tete.getValeur()).compareTo(m.getValeur()) > 0) {

				this.tete = new Maillon(x, this.tete);

			} else
				ajoutTrie(m, (Maillon<T>) this.tete);
			// Si la liste est vide on place directement l'objet en tete de
			// liste
		} else {
			Object l = new Maillon(x, null);
			this.tete = (Maillon) l;
			this.queue = (Maillon) l;
		}
	}

	/**
	 * Ajoute un maillon a partir de sa valeur a la liste chainee de maniere
	 * triee par rapport a un ordre defini dans la classe T. Methode auxillaire
	 * de sa methode homonyme
	 *
	 * @param m
	 *            Le maillon a ajouter.
	 *
	 * @param s
	 *            Le maillon a partir duquel on avance dans la liste.
	 * 
	 * @see LC#ajoutTrie(T)
	 */
	private <T extends Comparable<? super T>> void ajoutTrie(Maillon<T> m, Maillon<T> s) {
		// Si on arrive a la fin de la liste alors on ajouter le maillon a la
		// fin de la liste
		if (s.getSuivant() == null) {

			m.setSuivant(s.getSuivant());
			s.setSuivant(m);
			this.queue = (Maillon) m;
		} else {
			// Si l'element suivant a une valeur plus grande que la valeur qu'on
			// cherche a ajouter, on place la valeur a cet endroit de la liste

			if (s.getSuivant().getValeur().compareTo(m.getValeur()) > 0) {
				m.setSuivant(s.getSuivant());
				s.setSuivant(m);
			}
			// Sinon on continue de parcourir la liste
			else {
				ajoutTrie(m, s.getSuivant());
			}
		}
	}

	/**
	 * Ajoute un maillon a partir de sa valeur a la liste chainee de maniere
	 * triee par rapport a un ordre defini dans la classe T. Leve une erreur si
	 * un doublon est detecte dans la liste. Elle fait appel a une methode
	 * auxillaire.
	 *
	 * @param x
	 *            valeur du maillon a ajouter
	 * @throws Exception
	 *             Si il y a des doublons dans la liste
	 * @see LC#ajoutTrieSansDoublon(Maillon, Maillon)
	 */
	public <T extends Comparable<? super T>> void ajoutTrieSansDoublon(T x) throws Exception {
		Maillon<T> m = new Maillon<T>(x, null);

		if (!this.estListeVide()) {

			// Si deux objets sont les memes alors il y a doublon, on leve un
			// exception
			if (((Comparable<? super T>) this.tete.getValeur()).compareTo(m.getValeur()) == 0)
				throw new Exception("Presence de doublon dans la liste");
			// Si l'element de tete est plus grand que l'element a ajoute on
			// passe a la fonction auxillaire
			if (((Comparable<? super T>) this.tete.getValeur()).compareTo(m.getValeur()) > 0) {

				this.tete = new Maillon(x, this.tete);

			} else
				ajoutTrieSansDoublon(m, (Maillon<T>) this.tete);
			// Si la liste est vide on place directement l'objet en tete de
			// liste
		} else {
			Object l = new Maillon(x, null);
			this.tete = (Maillon) l;
			this.queue = (Maillon) l;
		}
	}

	/**
	 * Ajoute un maillon a partir de sa valeur a la liste chainee de maniere
	 * triee par rapport a un ordre defini dans la classe T. Leve une erreur si
	 * un doublon est detecte dans la liste. Methode auxillaire de sa methode
	 * homonyme
	 *
	 * @param m
	 *            Le maillon a ajouter.
	 *
	 * @param s
	 *            Le maillon a partir duquel on avance dans la liste.
	 * @throws Exception
	 *             Si il y a des doublons dans la liste
	 * @see LC#ajoutTrie(T)
	 */
	private <T extends Comparable<? super T>> void ajoutTrieSansDoublon(Maillon<T> m, Maillon<T> s) throws Exception {

		// Si on arrive a la fin de la liste alors on ajouter le maillon a la
		// fin de la liste
		if (s.getSuivant() == null) {

			m.setSuivant(s.getSuivant());
			s.setSuivant(m);
			this.queue = (Maillon) m;
		} else {
			// Si deux objets sont les memes alors il y a doublon, on leve un
			// exception
			if (s.getSuivant().getValeur().compareTo(m.getValeur()) == 0)
				throw new Exception("Presence de doublons dans la liste");
			// Si l'element suivant a une valeur plus grande que la valeur qu'on
			// cherche a ajouter, on place la valeur a cet endroit de la liste

			if (s.getSuivant().getValeur().compareTo(m.getValeur()) > 0) {
				m.setSuivant(s.getSuivant());
				s.setSuivant(m);
			}
			// Sinon on continu de parcourir la liste
			else {
				ajoutTrieSansDoublon(m, s.getSuivant());
			}
		}
	}

	/**
	 * Renvoie la valeur d'un maillon a un certain rang d'une liste chainee.
	 * Elle fait appel a une methode auxillaire
	 *
	 * @param i
	 *            Le rang ou est place la valeur qu'on veut connaitre
	 *
	 * @return La valeur non typee de la liste chainee que l'on veut connaitre
	 *
	 */
	public T get(int i) {

		if (!estListeVide() && this.taille() > i) {
			Maillon<T> m = this.tete;
			return get(i, m);
		} else
			return null;
	}

	/**
	 * Retourne la valeur du maillon d'un certain rang. Methode auxillaire de sa
	 * methode homonyme
	 *
	 * @param m
	 *            Un entier qui diminu a chaque appel recursif
	 *
	 * @param i
	 *            Le maillon a partir duquel on avance
	 * @return La valeur du maillon au rang selectionne
	 */

	private T get(int i, Maillon<T> m) {
		if (i == 0) {
			return m.getValeur();
		} else
			return get(i - 1, m.getSuivant());
	}

	/**
	 * Creer un iterateur sur la liste et le renvoie
	 * 
	 * @return L'iterateur cree sur la liste this
	 */
	public Iterator<T> iterator() {

		return new Iterator<T>(this.tete);
	}

	/**
	 * Retourne une representation sous forme de String de la classe LC
	 * 
	 * @return Une representation sous forme de String de la classe LC
	 */
	public String toString() {
		String toString = "";
		Maillon<T> maillon = this.getTete();
		while (maillon != null) {
			toString += maillon.getValeur().toString() + "\n";
			maillon = maillon.getSuivant();
		}
		return toString;
	}

}
