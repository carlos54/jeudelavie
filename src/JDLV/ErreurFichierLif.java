package JDLV;
/**
 * <b>ErreurFichierLif est la classe permettant de creer une exception en cas d'erreur de validite du fichier lif</b>
 *
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */

public class ErreurFichierLif extends Exception {
	/**
	 *Construit une nouvelle exception avec un message "fichier non valide".
	 */
	ErreurFichierLif() {
		super("Le fichier lif n'est pas valide, veuillez le modifier.\n");
	}

	/**
	 *Construit une nouvelle exception avec le message de detail specifie.
	 * @param cause la cause du message
	 */
	ErreurFichierLif(Throwable cause) {
		super("Le fichier lif n'est pas valide, veuillez le modifier.\n", cause);
	}
}
