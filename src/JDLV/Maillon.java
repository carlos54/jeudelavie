package JDLV;
/**
 * <b>Maillon est la classe permettant de creer un nouveau maillon a ajouter une
 * liste chainee</b>
 * 
 * 
 * Il dispose des champs suivants :
 * <ul>
 * 
 * <li>Une valeur de type non defini</li>
 * <li>Le maillon suivant de la liste</li>
 * </ul>
 * 
 * 
 * @see LC
 * 
 * @author Thomas Misiek, Didier Muhire, Charles Sayamath, Anthony Berduck
 */

public class Maillon<T> {
	/** Valeur du maillon de type non defini */
	private T valeur;
	/** Le maillon suivant de la chaine */
	private Maillon<T> suivant;

	/**
	 * Construit un maillon avec le maillon suivant et la valeur du maillon
	 * 
	 * @param valeur
	 *            Valeur du maillon
	 * @param suivant
	 *            Maillon suivant
	 */
	public Maillon(T valeur, Maillon<T> suivant) {
		this.valeur = valeur;
		this.suivant = suivant;
	}

	/**
	 * Retourne la valeur du maillon
	 * 
	 * @return La valeur du maillon
	 */
	public T getValeur() {
		return valeur;
	}

	/**
	 * Modifie la valeur du maillon
	 * 
	 * @param valeur
	 *            La nouvelle valeur du maillon.
	 */
	public void setValeur(T valeur) {
		this.valeur = valeur;
	}

	/**
	 * Retourne le maillon suivant du maillon this
	 * 
	 * @return Le maillon suivant
	 */
	public Maillon<T> getSuivant() {
		return suivant;
	}

	/**
	 * Modifie l'attribut suivant du maillon
	 * 
	 * @param suivant
	 *            Le nouveau maillon suivant
	 */
	public void setSuivant(Maillon<T> suivant) {
		this.suivant = suivant;
	}

	/**
	 * Renvoie une representation de Maillon sous forme de String
	 * 
	 * @return Representation du maillon sous forme de String
	 */
	public String toString() {
		return "Maillon [valeur=" + valeur + ", suivant=" + suivant + "]";
	}

}
