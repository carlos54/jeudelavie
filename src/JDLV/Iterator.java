package JDLV;


/**
 * <b>Iterator est la classe permettant d'iterer sur les maillons.</b>
 * 
 * <ul>
 * <li>Un attribut maillon</li>
 * </ul>
 * 
 *
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */


public class Iterator<T> {
	/**
	 * Le maillon itere a un moment donne
	 */
	private Maillon<T> maillon;

	/**
	 * Constructeur d'iterator
	 * @param maillon Le maillon a partir duquel on veut iterer
	 */
	public Iterator(Maillon<T> maillon) {
		this.maillon = new Maillon<T>(null, maillon);
	}

	/**
	 * Renvoie true si la liste n'a plus d'elements, sinon false
	 * @return true si la liste n'a plus d'elements, sinon false
	 */
	public boolean hasNext() {
		try {
			return this.maillon.getSuivant() != null;
		} catch (Exception E) {
			return false;
		}
	}

	/**
	 * Renvoie l'element suivant dans l'iteration.
	 * @return La valeur du maillon suivant
	 */
	public T next() {
		this.maillon = this.maillon.getSuivant();
		return this.maillon.getValeur();
	}

}
