package JDLV;

/**
 * <b>Generation est une classe abstraite. Elle est la representation a un
 * instant T du jeu.</b>
 * 
 * Generation est caracterise par les attributs suivants :
 * <ul>
 * <li>Un attribut Liste Cellules Vivantes permettant d'acceder aux cellules
 * vivantes du jeu.</li>
 * <li>Un attribut Numero Generation permettant de connaitre l'instant T du
 * jeu</li>
 * <li>Une liste de nombre de cellues voisines qu'une cellule vivante doit avoir
 * pour survivre le prochain tour</li>
 * <li>Une liste de nombre de cellues voisines qu'une cellule morte doit avoir
 * pour naitre le prochain tour</li>
 * </ul>
 * 
 *
 * @author Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 */

public abstract class Generation {
	/**
	 * liste chainee de cellules vivantes represantant le jeu a une generation
	 * donne
	 */
	protected LC<PositionCellule> listeCellulesVivantes;

	/**
	 * Une liste de nombre de cellues voisines qu'une cellule vivante doit avoir
	 * pour survivre le prochain tour
	 */
	protected LC<Integer> nbrSurvie;

	/**
	 * liste de nombre de cellues voisines qu'une cellule morte doit avoir pour
	 * naitre le prochain tour
	 */
	protected LC<Integer> nbrNaissance;
	/**
	 * Le numero de generation du jeu, il n'est pas possible de le modifier
	 * directement, juste de l'incrementer
	 */
	protected int numeroGeneration;

	/**
	 * Construit un objet Generation
	 * 
	 * @param lc
	 *            liste de cellules vivantes qui represente le jeu dans sa
	 *            premiere generation
	 * @param nbrSurvie
	 *            Une liste de nombre de cellues voisines qu'une cellule vivante
	 *            doit avoir pour survivre le prochain tour
	 * @param nbrNaissance
	 *            Une liste de nombre de cellues voisines qu'une cellule morte
	 *            doit avoir pour naitre le prochain tour
	 */
	public Generation(LC<PositionCellule> lc, LC<Integer> nbrSurvie, LC<Integer> nbrNaissance) {
		this.nbrSurvie = nbrSurvie;
		this.nbrNaissance = nbrNaissance;
		setListeCellulesVivantes(lc);
	}

	/**
	 * Methode abstraite pour creer une matrice decale elle est implementee dans
	 * les classes ModeInfini, ModeFrontiere et ModeCirculaire
	 * 
	 * @param le
	 *            decalage en x entre 1 et -1
	 * @param le
	 *            decalage en y entre 1 et -1
	 * @return La liste chainee decalee
	 * @see ModeCirculaire
	 * @see ModeFrontiere
	 * @see ModeInfini
	 */
	protected abstract LC<PositionCellule> creerMatriceDecale(int x, int y);

	/**
	 * Creer une nouvelle liste chainee, somme des deux listes chainees passees
	 * en parametre
	 * 
	 * <p>
	 * Cette methode permet de calculer la somme de deux listes triees en temps
	 * constant n + m avec n le nombre d'element de la liste 1 et m le nombre
	 * d'element de la liste 2
	 * </p>
	 * 
	 * @param lc1
	 *            premiere liste chainee
	 * @param lc2
	 *            seconde liste chainee
	 * @return la somme de deux matrices
	 */
	protected LC<PositionCellule> ajouterMatrice(LC<PositionCellule> lc1, LC<PositionCellule> lc2) {
		// la liste qui va representer l'addition des deux matrices lc1 et lc2
		// (implementees sous formes de liste chaines de cellules vivantes
		LC<PositionCellule> lcFinale = new LC<PositionCellule>();

		PositionCellule pc1 = null, pc2 = null;
		Maillon<PositionCellule> m2 = new Maillon<PositionCellule>(null, lc2.getTete()),
				m1 = new Maillon<PositionCellule>(null, lc1.getTete());

		if ((m2 != null) && (m1 != null)) {

			while (m2.getSuivant() != null) {

				m2 = m2.getSuivant();
				pc2 = m2.getValeur();
				// tant que la liste1 a encore des elements et que les elements
				// de la liste 2 sont plus grands ou egaux que ceux de la liste
				// 1
				while (m1.getSuivant() != null && pc2.compareTo(m1.getSuivant().getValeur()) >= 0) {
					m1 = m1.getSuivant();
					pc1 = m1.getValeur();
					// Si les deux elements iteres de la listes sont de
					// positions egales, on ajoute leurs valeurs et on ajouter
					// la cellule avec les deux valeurs ajoutes dans la liste
					// finale
					if (pc2.compareTo(pc1) == 0) {
						pc2.ajouterValeurCellule(pc1);
						lcFinale.ajouterEnQueue(pc2);
						// sinon si l'element de la liste 2 est plus grand que
						// celui de la liste 1 alors on ajoute l'element de la
						// liste 1 a la liste finale
					} else

					if (pc2.compareTo(pc1) > 0 || (!(m2.getSuivant() != null) && pc2.equals(pc1))) {
						lcFinale.ajouterEnQueue(pc1);
					}
				} // Si la liste 1 n'a plus d'element on fini par ajouter tous
					// les elements de la liste 2 dans la liste finale
				if ((pc1 == null) || pc1 != null && pc2.compareTo(pc1) != 0) {
					lcFinale.ajouterEnQueue(pc2);

				}
			}
			// Si la liste 2 n'a plus d'element on fini par ajouter tous les
			// elements de la liste 1 dans la liste finale
			while (m1.getSuivant() != null) {
				m1 = m1.getSuivant();
				lcFinale.ajouterEnQueue(m1.getValeur());
			}
		} //

		// on retourne la somme des deux matrices
		return lcFinale;

	}

	/**
	 * Calcul le nombre de voisins pour chaque cellule
	 * 
	 * @return Une liste chainee de PositionCellule dont la valeur est le nombre
	 *         de voisins d'une cellule a une position donnee par les attributs
	 *         x et y de la PositionCellule
	 */
	protected LC<PositionCellule> listeVoisinsVivants() {
		LC<PositionCellule> lc = this.listeCellulesVivantes;

		// cette double boucle for imbriquee permet de decaler de 1 pas la
		// matrices dans tous les axes, diagonales comprises
		for (int x = -1; x <= 1; x++)
			for (int y = -1; y <= 1; y++) {

				if (!(x == 0 && y == 0)) {

					LC<PositionCellule> mat = creerMatriceDecale(x, y);
					// on ajoute deux matrices dont le resultat est stocke dans
					// lc puis on renvoie lc vers l'ajout de matrice 8 fois,
					// elle "grossi" de plus en plus a chaque fois
					lc = ajouterMatrice(mat, lc);
				}

			}

		return lc;
	}

	/**
	 * Return true si une cellule va vivre au tour suivant et false sinon par
	 * rapport aux regles du jeu (#N #R)
	 * 
	 * @param valeur
	 *            d'une cellule, represente le nombre de ses voisins vivantes au
	 *            tour precedent
	 * @see ModeInfini#successeur()
	 * @see ModeFrontiere#successeur()
	 * @see ModeCirculaire#successeur()
	 */
	protected boolean vivreOuPas(int i) {

		Iterator<Integer> it = this.nbrSurvie.iterator();
		// on verifie par rapport aux cellules vivantes au tour dernier
		while (it.hasNext()) {
			// les cellules vivantes au tour dernier ont toutes une valeur au
			// moins egale a 10
			if (it.next() + 10 == i)
				return true;

		}

		it = this.nbrNaissance.iterator();
		// on verifie par rapport aux cellules mortes au tour dernier
		while (it.hasNext()) {

			if (it.next() == i)
				return true;

		}

		return false;

	}

	/**
	 * <p>
	 * Retourne la generation suivante du jeu et modifie automatiquement la
	 * liste des cellules vivantes de la generation pour lui affecter les
	 * Cellules vivantes de la generation suivante.
	 * </p>
	 * 
	 * @return Le jeu dans sa nouvelle generation
	 */

	public Generation successeur() {
		LC<PositionCellule> lc2 = new LC<PositionCellule>();
		LC<PositionCellule> lc = listeVoisinsVivants();
		Iterator<PositionCellule> it = lc.iterator();
		while (it.hasNext()) {
			PositionCellule l = (PositionCellule) it.next();
			if (vivreOuPas(l.getVal())) {
				// on donne aux cellules vivantes d prochain tour une valeur de
				// 10 pour pouvoir les differencier de celles qui etaient mortes
				lc2.ajouterEnQueue(new PositionCellule(l.getX(), l.getY(), 10));
			}
		}
		this.listeCellulesVivantes = lc2;
		this.numeroGeneration++;
		return this;
	}

	/**
	 * Accesseur de la liste des cellules vivantes
	 * 
	 * @return La liste chainee representant la matrice du jeu a une generation
	 *         donnee
	 */
	public LC<PositionCellule> getListeCellulesVivantes() {
		return listeCellulesVivantes;
	}

	/**
	 * Accesseur du numero de generation
	 * 
	 * @return numero de generation
	 */
	public int getNumeroGeneration() {
		return this.numeroGeneration;
	}

	/**
	 * Mutateur de la liste de cellules vivantes
	 * 
	 * @param listeCellulesVivantes
	 *            La liste chainee representant la matrice du jeu a une
	 *            generation donnee
	 */
	public void setListeCellulesVivantes(LC<PositionCellule> listeCellulesVivantes) {
		this.listeCellulesVivantes = listeCellulesVivantes;
	}

	/**
	 * <p>
	 * Une methode abstraite permettant de faire une copie de la generation
	 * ayant toute les valeurs d'une autre generation mais ne pointant pas vers
	 * elle
	 * </p>
	 * .
	 * 
	 * @return La generation copie
	 */
	public abstract Generation getCopie();

	/**
	 * Repete un string un certaine nombre de fois.
	 * 
	 * @param str
	 *            String a repeter
	 * @param nbr
	 *            Nombre de fois que le String doit etre repete
	 * @return Le generation copie qui ne pointe pas vers this
	 * @see Generation#toString()
	 */
	public String repeatChar(String str, int nbr) {

		if (nbr <= 0)
			return "";
		return str + repeatChar(str, nbr - 1);
	}

	/**
	 * Retourne une representation de l'objet Generation sous forme de String
	 * <p>
	 * Fonctionne avec tous les modes de jeun'affiche pas les cellules mortes,
	 * ni de cadre autour du jeu
	 * </p>
	 * 
	 * @return Une representation de l'objet Generation sous forme de String
	 */
	public String toString() {

		// On cherche les deux plus petites position en abscisses et en
		// ordonnees des cellules vivantes dans le jeu
		Iterator<PositionCellule> it = this.listeCellulesVivantes.iterator();
		int min = 1000000000;
		while (it.hasNext()) {
			int valeur = it.next().getX();
			if (valeur < min)
				min = valeur;
		}
		String s = "";
		it = this.listeCellulesVivantes.iterator();
		PositionCellule precedent = null;
		boolean premiereLigne = true;
		while (it.hasNext()) {
			PositionCellule m = (PositionCellule) it.next();
			// si on est a la premier ligne il faut afficher un certain un
			// certain nombre d'espaces avant la premiere cellule, ce nombre est
			// donne par Math.abs(min - m.getX())
			if (premiereLigne == true) {
				s += repeatChar(" ", Math.abs(min - m.getX()));
				premiereLigne = false;
				precedent = m;
			} else {
				if (m.getY() == precedent.getY()) {
					// entre chaques cellules vivantes sans saut de
					// lignes on affiche le nombre d'espace egal a
					// m.getX() - precedent.getX() - 1
					s += repeatChar(" ", m.getX() - precedent.getX() - 1);
				} else {
					// Si la cellule suivante n'est pas sur la meme ligne que la
					// precedente, on saute autant de ligne qu'il y a de
					// difference entre m.getY() et precedent.getY()
					s += repeatChar("\n", m.getY() - precedent.getY());
					// Si on a saute une ligne et qu'une cellule vivante se
					// trouve a la ligne suivante, il faut afficher un certains
					// nombre d'espace avant la cellule vivante , ce nombre est
					// donne par Math.abs(min - m.getX())
					s += repeatChar(" ", Math.abs(min - m.getX()));
				}
				precedent = m;
			}
			s += "*";
		}
		return s += "\nGeneration numero: " + this.getNumeroGeneration() + "\n";

	}

}
