package JDLV;

import java.io.File;
/**
 * <b>FolderFilter est la classe permettant de creer un filtre qui n'accepte
 * que les dossier</b>
 *<p>Elle est utilisee pour ne pas afficher les fichiers avec JFileChooser</p>
 * @authors Thomas Misiek,Charles Sayamath, Didier Muhire, Anthony Berduck
 * @see TraitementDossierHtml#enregistrerHtml(String)
 */

class FolderFilter extends javax.swing.filechooser.FileFilter {
	  @Override
	  public boolean accept( File file ) {
	    return file.isDirectory();
	  }

	  @Override
	  public String getDescription() {
	    return "Selectionnez un fichier";
	  }


	}