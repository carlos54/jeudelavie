package JDLV;

/**
 * <b>Bornes est la classe permettant de definir les bornes d'une grille de jeu</b>
 * 4 champs la caracterisent :
 * <ul>
 * <li>Un entier xInf, borne inferieure de l'axe x</li>
 * <li>Un entier xSup, borne superieure de l'axe x</li>
 * <li>Un entier yInf, borne inferieure de l'axe y</li>
 * <li>Un entier ySup, borne superieure de l'axe y</li>
 * </ul>
 * 
 * @author Thomas Misiek, Charles Sayamath, Didier Muhire, Anthony Berduck
 * 
 */

public class Bornes {

	/**Borne inferieure de l'axe x*/
	private int xInf;
	/**Borne superieure de l'axe x*/
	private int xSup;
	/**Borne inferieure de l'axe y*/
	private int yInf;
	/**Borne superieure de l'axe y*/
	private int ySup;

	/**Construit un objet de type Bornes avec 4 bornes
	 * @param x1
	 * 			Borne inferieure de l'axe x
	 * @param x2
	 * 			Borne superieure de l'axe x
	 * @param y1
	 * 			Borne inferieure de l'axe y
	 * @param y2
	 * 			Borne superieure de l'axe y
	 * */
	public Bornes(int x1, int x2, int y1, int y2) {

		if (x1 > x2) {
			this.xInf = x2;
			this.xSup = x1;
		} else {
			this.xInf = x1;
			this.xSup = x2;
		}
		if (y1 > y2) {
			this.yInf = y2;
			this.ySup = y1;
		} else {
			this.yInf = y1;
			this.ySup = y2;
		}

	}

	/**
	 * Retourne la valeur de la borne inferieure de l'axe x
	 * @return la valeur de la borne inferieure de l'axe x
	 */
	public int getXInf() {
		return xInf;
	}
	
	/**
	 * Retourne la valeur de la borne superieure de l'axe x
	 * @return la valeur de la borne superieure de l'axe x
	 */
	public int getXSup() {
		return xSup;
	}

	/**
	 * Retourne la valeur de la borne inferieure de l'axe y
	 * @return la valeur de la borne inferieure de l'axe y
	 */
	public int getYInf() {
		return yInf;
	}
	
	/**
	 * Retourne la valeur de la borne superieure de l'axe y
	 * @return la valeur de la borne superieure de l'axe y
	 */
	public int getYSup() {
		return ySup;
	}

	/**
	 * Verifie si la cellule est contenue dans les bornes
	 * @param pc pc
	 * 			Donne la position de la cellule
	 * @return true si la cellule est contenue dans les bornes et false sinon
	 */
	public boolean contient(PositionCellule pc) {
		return (pc.getX() >= this.xInf) && (pc.getX() <= this.xSup) && (pc.getY() >= this.yInf)
				&& (pc.getY() <= this.ySup);

	}

	/**
	 * Retourne une representation d'un Objet de type Bornes sous forme de String
	 * @return Representation de Bornes sous forme de String
	 */
	@Override
	public String toString() {
		return "Bornes [Abscisse inferieure=" + xInf + ", Abscisse inferieure" + xSup + ", Ordonnee inferieure=" + yInf
				+ ", Ordonnee superieur=" + ySup + "]";
	}

}
