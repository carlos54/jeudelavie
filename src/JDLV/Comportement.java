package JDLV;

/**
 * <b> Comportement permet de connaitre le comportement du jeu </b>
 * 
 * Il dispose des champs suivants :
 * <ul>
 * <li>Une duree de type entier</li>
 * <li>Une queue de type entier</li>
 * <li>Une periode de type entier</li>
 * <li>Un deplacement en abscisse de type entier</li>
 * <li>Un deplacement en ordonnees de type entier</li>
 * <li>Un type de type String</li>
 * <li>Un jeu analyse de type Jeu</li>
 * </ul>
 * 
 * 
 * @see Jeu
 * 
 * @author Thomas Misiek, Didier Muhire, Charles Sayamath, Anthony Berduck
 */

public class Comportement {
	/** La duree maximale d"analyse */
	private int duree;
	/** Une longueuer de la queue avant que le jeu ne devienne periodique */
	private int queue;
	/** La periode entre deux meme generations du jeu */
	private int periode;
	/** Le deplacement en abscisse */
	private int deplacementX;
	/** Le deplacement en ordonnees */
	private int deplacementY;
	/** Le type d'evolution */
	private String type;
	/** Le jeu analyse */
	private Jeu jeuAnalyse;

	/**
	 * Constructeur d'un Comportement a partir d'un jeu et d'une duree
	 * 
	 * @param jeu
	 *            Le jeu analyse
	 * @param duree
	 *            La duree maximale de l'analyse
	 */
	public Comportement(Jeu jeu, int duree) {
		jeuAnalyse = jeu;
		this.comportement(duree);

	}

	/**
	 * Retourne la longueur de la queue du jeu d'un type entier
	 * 
	 * @return queue
	 */
	public int getQueue() {
		return this.queue;
	}

	/**
	 * Retourne la periode du jeu
	 * 
	 * @return periode
	 */
	public int getPeriode() {
		return this.periode;
	}

	/**
	 * Retourne le deplacement en abscisse du jeu
	 * 
	 * @return deplacement
	 */
	public int getDeplacementX() {
		return this.deplacementX;
	}

	/**
	 * Retourne le deplacement du jeu en ordonnees
	 * 
	 * @return deplacement
	 */
	public int getDeplacementY() {
		return this.deplacementY;
	}

	/**
	 * Retourne le type du jeu sous forme de String
	 * 
	 * @return type
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Retourne la duree maximale de l'analyse
	 * 
	 * @return La duree maximale de l'analyse
	 */
	public int getDuree() {
		return this.duree;
	}

	/**
	 * Calcul de la periode d'un jeu a partir d'une generation
	 * 
	 * @return periode La periode du jeu
	 */
	private int periode(Generation j) {

		Generation j2 = j.getCopie();
		j.successeur();
		int n = 1;
		while (memeConfig(j, j2) < 0) {

			n++;
			j.successeur();
		}
		return n;

	}

	/**
	 * Calcul de la longueur de la queue a partir de deux generations
	 * 
	 * @return queue La longueur de la queue du jeu
	 */
	private int queue(Generation j, Generation j2) {
		int queue = 0;
		while (memeConfig(j, j2) < 0) {

			j.successeur();
			queue++;
		}
		return queue;

	}

	/**
	 * Calcule si deux listes de cellule vivantes ont la meme configuration
	 * 
	 * @param j2
	 * @return Retourne 0 si c'est exactement la meme configuration, retourne un
	 *         nombre positif si c'est la meme configuration decale de ce meme
	 *         nombre positif. Retourne -1 si ce n'est pas la meme configuration
	 */
	private int memeConfig(Generation j1, Generation j2) {

		Iterator<PositionCellule> it = j1.getListeCellulesVivantes().iterator();
		Iterator<PositionCellule> it2 = j2.getListeCellulesVivantes().iterator();
		int decalageX = 0;
		int decalageY = 0;
		// cette methode ne peut retourner deux entiers en meme teps, elle donne
		// donc a decalageX et decalageY leurs valeur par effet de bord
		this.deplacementX = -1;
		this.deplacementY = -1;
		boolean premierTour = true;

		while (it.hasNext() && it2.hasNext()) {

			PositionCellule pc = (PositionCellule) it.next();
			PositionCellule pc2 = (PositionCellule) it2.next();
			// Si c'est le premier tour le decalage peut avoir n'importe quelle
			// valeur, on l'initialise donc
			if (premierTour) {
				premierTour = false;
				decalageX = (pc.getX() - pc2.getX());
				decalageY = (pc.getY() - pc2.getY());

				// Maintenant que le decalage est fixe, il ne faut plus qu'il
				// change, sinon la configuration aura change
			} else {
				if (!(decalageX == (pc.getX() - pc2.getX())))
					return -1;
				if (!(decalageY == (pc.getY() - pc2.getY())))
					return -1;
			}

		}
		// Si les deux listes n'ont pas les meme longueurs, elles ne peuvent
		// etre de eme configuration
		if (it.hasNext() || it2.hasNext()) {

			return -1;
		}
		this.deplacementX = decalageX;
		this.deplacementY = decalageY;
		// on retourne un des deux decalage au choix, les deux decalages sont de
		// toutes facons envoyes en attribut de la classe par effet de bord
		if (decalageY != 0)
			return Math.abs(decalageY);
		return Math.abs(decalageX);
	}

	/**
	 * Modifie les variables d'instances pour traduire le comportement de
	 * jeuAnalyse sur une succession de 20 generations
	 * 
	 * @param iteration
	 *            nb d'iteration de type entier
	 */
	public void comportement(int iteration) {
		this.duree = iteration;
		Generation generation1 = this.jeuAnalyse.getGenerationDeBase().getCopie();
		Generation generation2 = this.jeuAnalyse.getGenerationDeBase().getCopie().successeur();

		int i = 0;

		// on essaye de trouver une meme configuration entre deux generations
		// qui avance des a vitesse differentes, si la duree arrive a son terme
		// le jeu est de type inconnu, si avant cela on trouve deux generation
		// ayant une meme configuration, alors le jeu aura un type connu qui
		// dependera de sa periode et queue
		do {
			i++;
			generation1.successeur();
			generation2.successeur().successeur();
		} while ((memeConfig(generation1, generation2) < 0) && i < iteration);
		int decalage = memeConfig(generation1, generation2);

		if (decalage >= 0) {
			this.queue = queue(this.jeuAnalyse.getGenerationDeBase().getCopie(), generation1);

			this.periode = periode(generation1);
			if (generation1.listeCellulesVivantes.estListeVide()) {
				this.type = "Mort";

			} else {

				;
				if (decalage == 0 && periode == 1) {
					this.type = "Stable";
				} else {
					if (decalage == 0) {
						this.type = "Periodique";

					} else
						this.type = "Vaisseau";
				}
			}
		} else {// Les attributs de la classe sont modifiees par effet de bord
				// et non pas par valeur de retour, une valeur de -1 ne
				// correspond a rien de reel, c'est un code d'erreur
			this.periode = -1;
			this.queue = -1;
			this.type = "Inconnu";
		}

	}

	/**
	 * Retourne une representation d'un comportement sous forme de String
	 * 
	 * @return Une representation sous forme de String d'un objet comportement
	 * 
	 */
	public String toString() {
		String s = "";
		s += ("Comportement du jeu \"" + this.jeuAnalyse.getFileName() + " \"calcule en " + this.duree
				+ " generations\n");
		s += ("Type: " + this.type + "\n");
		if (this.queue > -1) {
			s += ("Queue: " + this.queue + "\n");
			s += ("Periode: " + this.periode + "\n");
			s += ("Deplacement en abscisse: " + this.deplacementX + "\n");
			s += ("Deplacement en ordonnees: " + this.deplacementY + "\n");

		}
		return s;
	}
}
