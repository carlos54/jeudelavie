package JDLV;

/**
 * <b>PositionCellule est une classe permettant de creer une cellule disposant
 * de coordonnees et d'une valeur</b>
 * 
 *
 * Elle de deux coordonees et d'une valeur :
 * <ul>
 * 
 * <li>Un entier x, position de la cellule en abscisse</li>
 * <li>Un entier y, position de la cellule en ordonne</li>
 * <li>Un entier val, valeur de la cellule</li>
 * </ul>
 * 
 * 
 * 
 * 
 * * @authors Thomas Misiek, Didier Muhire, Charles Sayamath, Anthony Berduck
 */
public class PositionCellule implements Comparable<PositionCellule> {
	/**
	 * La position de la cellule en abscisse
	 */
	private int x;
	/**
	 * La position de la cellule en ordonnee
	 */
	private int y;
	/**
	 * * La valeur de la cellule
	 */
	private int val;

	/**
	 * Construit une nouvelle position cellule et initialise ses trois attributs
	 * 
	 * @param x
	 *            La position de la cellule en abscisse
	 * @param y
	 *            La position de la cellule en ordonnee
	 * @param val
	 *            La valeur de la cellule
	 */
	public PositionCellule(int x, int y, int val) {

		this.x = x;
		this.y = y;
		this.val = val;

	}

	/**
	 * Accesseur de la position en abscisse de la cellule de la cellule
	 * 
	 * @return Position en abscisse de la celle
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Accesseur de la position en ordonnee de la Cellule
	 * 
	 * @return Position en ordonnee de la cellule
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * Accesseur de la valeur de la cellule
	 * 
	 * @return La valeur de la cellule
	 */
	public int getVal() {
		return this.val;
	}

	/**
	 * Ajouter la valeur d'une cellule a une autre
	 * 
	 * @param pc
	 *            Valeur de la cellule a ajouter a this
	 */
	public void ajouterValeurCellule(PositionCellule pc) {
		this.val += pc.getVal();
	}

	/**
	 * Renvoie true si les cellules sont a la meme position, false sinon
	 * 
	 * @param pc
	 *            Position de la cellule a comparer a this
	 * @return true si les deux cellules ont meme position false sinon
	 */
	public boolean memePosition(PositionCellule pc) {
		return (pc.getX() == this.x && pc.getY() == this.y);
	}

	/**
	 * Determine si un objets et this sont les meme
	 * 
	 * @param obj
	 *            L'objet a comparer avec this
	 * @return true si this est le meme objet que this false sinon
	 */
	@Override
	public boolean equals(Object obj) {
		PositionCellule pc = (PositionCellule) obj;
		if (pc.getX() == this.x && pc.getY() == this.y)
			return true;
		return false;
	}

	/**
	 * Compare une cellule avec thisen fonction de leurs coordonnees, retourne
	 * un entier en fonction du resultat
	 * <p>
	 * y est plus important que x, plus grand sont x est y , plus grande est la
	 * position de la cellule
	 * </p>
	 * 
	 * @param arg0
	 *            Cellule a comparer avec this
	 * @return 1 si this est plus grand que arg0, 0 si ils ont meme valeur et -1
	 *         si arg0 est plus grand que this (avec y plus important que x)
	 */

	@Override
	public int compareTo(PositionCellule arg0) {
		if (arg0.getY() > y)
			return -1;
		if (arg0.getY() < y)
			return 1;
		if (arg0.getX() > x)
			return -1;
		if (arg0.getX() < x)
			return 1;

		return 0;
	}

	/**
	 * Retourne une representation de l'objet Position Cellule sous forme de
	 * String
	 * 
	 * @return Une representation de l'objet Position Cellule sous forme de
	 *         String
	 */
	@Override
	public String toString() {
		return "PositionCellule [x=" + x + ", y=" + y + ", " + val + "]";
	}
}