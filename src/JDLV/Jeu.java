package JDLV;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * <b>La classe Jeu permet de creer un jeu qui va lire un fichier lif et
 * instancier une generation et qui pourra determiner le comportement d'un jeu
 * de la vie en instanciant un objet de classe Comportement </b>
 * Elle a plusieurs attributs
 * <ul>
 * <li>Une generation du jeu de type Generation</li>
 * <li>Une generation de base de type Generation</li>
 * <li>Un comportement du jeu de type Comportement</li>
 * <li>Un nom de fichier .lif d'ou provient le jeu de type String
 * <li>
 * <li>Un mode de jeu de type String (Circulaire, infini, frontiere)
 * <li>
 * </ul>
 * 
 * @see Generation
 * @see Comportement
 * 
 * @author Didier Muhire, Charles Sayamath, Thomas Misiek, Anthony Berduck
 */

public class Jeu {
	/** Generation du jeu de type Generation a un moment donne */
	private Generation generation;

	/** Premier Generation du jeu, elle permet de calculer le comportement */
	private Generation generationDeBase;
	/** Comportement du jeu de type Comportement */
	private Comportement comportement;
	/** Nom du fichier .lif d'ou provient le jeu de type String */
	private String fileName;
	/** Mode de jeu de type String */
	private String mode;

	/**
	 * Une liste de nombre de cellues voisines qu'une cellule vivante doit avoir
	 * pour survivre le prochain tour
	 */
	protected LC<Integer> nbrSurvie;

	/**
	 * liste de nombre de cellues voisines qu'une cellule morte doit avoir pour
	 * naitre le prochain tour
	 */
	protected LC<Integer> nbrNaissance;

	/**
	 * 
	 * /** Constructeur d'un jeu avec 4 parametres :
	 * 
	 * 
	 * @param filePath
	 *            Chemin d'acces du fichier lif
	 * 
	 * @param mode
	 *            mode du jeu(infini,frontiere,circulaire)
	 * @param bornes
	 *            les Bornes du jeu, elles peuvent etre null si le jeu est de
	 *            mode infini
	 */
	public Jeu(File filePath, String mode, Bornes bornes) {
		this.mode = mode;
		this.fileName = filePath.getName();
		// on donne a this.comportement la valeur null car il pourrait etre tres
		// long de le calculer, il ne servira que si c'est demande dans le main
		this.comportement = null;
		this.nbrSurvie = new LC<Integer>();
		this.nbrNaissance = new LC<Integer>();

		try {

			switch (mode) {

			// On se sert de nbrSurvie et nbrNaissance comme des pointeurs, on
			// les passe en argument, ils seront modifies mais pas retournes
			case "frontiere":
				this.generation = new ModeFrontiere(transformerLifEnPosition(filePath, nbrSurvie, nbrNaissance), bornes,
						nbrSurvie, nbrNaissance);
				break;
			case "circulaire":
				this.generation = new ModeCirculaire(transformerLifEnPosition(filePath, nbrSurvie, nbrNaissance),
						bornes, nbrSurvie, nbrNaissance);
				break;
			default:
				this.generation = new ModeInfini(transformerLifEnPosition(filePath, nbrSurvie, nbrNaissance), nbrSurvie,
						nbrNaissance);
				break;

			}
			// Si le fichier lif n'est pas au bon format (trop de commentaires,
			// caractere innattendu et 10 autres erreurs, une exception est
			// levee et le programme s'arrete avec un message d'erreur et la
			// valeur de retour 1
		} catch (Exception e) {
			System.out.println("Fichier: " + fileName + " " + e.getMessage() + e.getCause().getMessage());
			System.exit(1);
		}
		this.generationDeBase = this.generation;
	}

	/**
	 * Retourne le mode de jeu
	 * 
	 * @return le mode de jeu de type String (infini,frontiere,circulaire)
	 */
	public String getMode() {
		return this.mode;
	}

	/**
	 * Retourne et instancie le comportement du jeu
	 * 
	 * @param duree
	 *            maximale de calcul du comportement
	 * @return le mode de jeu de type String (infini,frontiere,circulaire)
	 * @see Comportement
	 */
	public Comportement getComportement(int duree) {
		if (this.comportement == null || this.comportement.getDuree() == duree)
			this.comportement = new Comportement(this, duree);
		return this.comportement;

	}

	/**
	 * Retourne la generation actuelle du jeu
	 * 
	 * @return generation de type Generation
	 */
	public Generation getGeneration() {
		return this.generation;
	}

	/**
	 * Retourne la generation de base du jeu
	 * 
	 * @return generation de type Generation
	 */
	public Generation getGenerationDeBase() {
		return this.generationDeBase;
	}

	/**
	 * Retourne le nom du fichier .lif d'ou provient le jeu
	 * 
	 * @return le nom du fichier .lif d'ou provient le jeu sous forme de String
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Creer une liste chainee de PositionCellule a partir d'un fichier .lif
	 * 
	 * @param file
	 *            fichier .lif de type File
	 * 
	 * @param nbrSurvie
	 *            Une liste de nombre de cellues voisines qu'une cellule vivante
	 *            doit avoir pour survivre le prochain tour
	 * @param nbrNaissance
	 *            liste de nombre de cellues voisines qu'une cellule morte doit
	 *            avoir pour naitre le prochain tour
	 * 
	 * 
	 * @return Une liste chainee de position cellule de type LC
	 * 
	 * @throws ErreurFichierLif 
	 * 				Si le fichier lif n'est pas valide
	 */
	public static LC<PositionCellule> transformerLifEnPosition(File file, LC<Integer> nbrSurvie,
			LC<Integer> nbrNaissance) throws ErreurFichierLif {
		String ligne = "";
		LC<PositionCellule> lc = new LC<PositionCellule>();

		int compteurCommentaire = 0;
		int y = 0;
		int init_lif_x = 0;
		int init_lif_y = 0;
		String[] ligneP;
		try {
			BufferedReader lifFile = new BufferedReader(new FileReader(file));

			ligne = lifFile.readLine();
			if (!ligne.matches(".*Life.*")) {
				throw new ErreurFichierLif(new Throwable("Pas de premiere ligne, ou bien premiere ligne mal ecrite"));
			}

			while ((ligne = lifFile.readLine()) != null) {
				// Si la ligne est un commentaire
				if (ligne.matches("^#D.*")) {
					compteurCommentaire++;
					if (compteurCommentaire > 22) {
						throw new ErreurFichierLif(
								new Throwable("Il y a " + compteurCommentaire + " commentaires. La limite est de 22."));
					}

				} else
					break;

			}

			// Si la ligne est la definition des regles
			if (ligne.matches("^#R\\s.*") || (ligne.matches("^#N"))) {
				// si ce sont les regles normales
				if (ligne.matches("^#N")) {

					nbrSurvie.ajouterEnQueue(2);
					nbrSurvie.ajouterEnQueue(3);
					nbrNaissance.ajouterEnQueue(3);
				} else {
					String[] p = ligne.split("/");

					if (p.length > 2 || p.length < 2 || p[0].length() < 4 || p[1].length() == 0) {
						throw new ErreurFichierLif(new Throwable("La definition des regles du jeu est mal ecrite"));
					}
					// pour les regles concernant le nombre de cellules voisines
					// qu'une cellule vivante doit avoir
					for (int x = 3; (p[0].length() > x); x++) {
						if (Character.isDigit(p[0].charAt(x))) {
							nbrSurvie.ajouterEnQueue(Character.getNumericValue(p[0].charAt(x)));
						} else {
							throw new ErreurFichierLif(
									new Throwable("Charactere non autorise dans la definition des regles du jeu"));
						}

					}
					// pour les regles concernant le nombre de cellules voisines
					// qu'une cellule morte doit avoir
					for (int x = 0; (p[1].length() > x); x++) {
						if (Character.isDigit(p[1].charAt(x))) {
							nbrNaissance.ajouterEnQueue(Character.getNumericValue(p[1].charAt(x)));
						} else {
							throw new ErreurFichierLif(
									new Throwable("Charactere non autorise dans la definition des regles du jeu"));
						}

					}

				}
				ligne = lifFile.readLine();
			}
			while (ligne != null) {
				// Si la ligne est un parametrage des positions
				// initiliales de la premiere cellule
				if (ligne.matches("^#P\\s.*")) {

					ligneP = ligne.split(" ");

					try {// on lit les positions des origines
						init_lif_x = Integer.parseInt(ligneP[1]);
						init_lif_y = Integer.parseInt(ligneP[2]);
					} catch (NumberFormatException e) {
						throw new ErreurFichierLif(new Throwable(
								"Les origines des blocs de cellules ne sont pas tous composees d'entiers"));
					}

					if (ligneP.length > 3)
						throw new ErreurFichierLif(new Throwable("Il y a trop de parametre a la ligne de parametrage"));

					y = 0;
					while ((ligne = lifFile.readLine()) != null) {

						if (ligne.matches("^\\..*") || ligne.matches("^\\*.*")) {

							if (ligne.length() > 80)
								throw new ErreurFichierLif(new Throwable("Trop de cellules dans une ligne"));
							for (int x = 0; x < ligne.length(); x++) {
								if (ligne.charAt(x) == '*') {

									try {
										lc.ajoutTrieSansDoublon(
												new PositionCellule(x + init_lif_x, y + init_lif_y, 10));
									} catch (Exception e) {
										throw new ErreurFichierLif(
												new Throwable("Deux blocs de cellules se chevauchent"));
									}
									// si le charactere est autre chose qu'un
									// point ou qu'une etoile, il n'est pas
									// autorise on leve une erreur
								} else if (ligne.charAt(x) != '.') {
									throw new ErreurFichierLif(new Throwable(
											"Caractere non autorise dans un bloc de cellule : " + ligne.charAt(x)));
								}

							}
							y++;

						} else {

							break;

						}
					}
				} else
					throw new ErreurFichierLif(new Throwable("Caractere non autorise ou liste vide dans le fichier"));

			}

			lifFile.close();
		} catch (IOException e) {
			throw new ErreurFichierLif(new Throwable("Le fichier passe en argument n'existe pas"));
		}

		return lc;
	}

	/**
	 * Renvoie une representation sous forme de String d'un objet Jeu
	 * 
	 * @return Une representation sous forme de String d'un objet Jeu
	 */
	@Override
	public String toString() {
		return "Jeu [generation de base \n " + generationDeBase + "\n generation actuelle \n=" + generation
				+ ", comportement=" + comportement + ", nom du fichier=" + fileName + ", mode=" + mode + "]";
	}

}